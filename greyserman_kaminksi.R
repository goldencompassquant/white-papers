#################################################
# Study on Greyserman-Kaminkski
#################################################
rm(list = ls())

# required packages
library(xts)
library(ggplot2)
library(grid)
library(gridExtra)
library(dplyr)
library(zoo)
library(quantmod)
library(TTR)
library(devtools)
library(Quandl)
library(httr)
library(scales)
library(gtable)

# load data from dropbox
load("C:/Users/Clement/Dropbox/Golden Compass/Bloomberg Data Repository/Daily OHLC/Weekly Price Indicators Refresh - 2017-08-03.RData")

ret <- NULL
snr <- NULL

selection <- c(4,6,9)

for(i in selection){
# select security
df.prices <- bars.repo[[i]]
df.prices <- as.xts(read.zoo(df.prices))
colnames(df.prices) <- c("open","high","low","close","volume","oi")
df.prices <- na.omit(df.prices$close)

# signal to noise ratio
df.prices$abs_diffs <- abs(diff(df.prices$close, lag=99))
df.prices$sum_movements <- rollapply(df.prices$close, 100, function(z) sum(abs(diff(z)), na.rm = TRUE))
df.prices$snr <- df.prices$abs_diffs / df.prices$sum_movements

# trend following strategy
df.prices$signal <- (EMA(df.prices$close, n=50)-EMA(df.prices$close, n=100)) / runSD(df.prices$close)
df.prices$ret <- ROC(df.prices$close)
df.prices$trade <- lag(ifelse(df.prices$signal>=0,1,-1)) # binary
df.prices$pl <- df.prices$trade * df.prices$ret
df.prices$cr <- 1+df.prices$pl
df.prices$snr <- lag(df.prices$snr)
df.prices <- na.omit(df.prices)

# compare roll ret to mdi
df.prices$cr <- cumprod(df.prices$cr)
df.prices$rollret <- ROC(df.prices$cr, n=99)
df.prices <- na.omit(df.prices)

# print(cor(df.prices$rollret, df.prices$snr))
ret <- cbind(ret, df.prices$rollret)
snr <- cbind(snr, df.prices$snr)

}

colnames(ret) <- colnames(snr) <- names(bars.repo)[selection]

ret <- na.omit(ret)
ret$avg <- rowMeans(ret)

snr <- na.omit(snr)
snr$mdi <- rowMeans(snr)

cor(rowMeans(ret),rowMeans(snr))  

plots <- list()

nameVec <- c("CME COMEX Gold","LME Copper","ICE Brent","MDI")

for(j in 1:3){
  
  p1 <- ggplot() +
    geom_line(aes(x= index(snr), y=snr[,j]), color = "blue", size = 1) +
    scale_x_date(expand = c(0,0)) +
    scale_y_continuous(limits = c(0,.5) ,expand = c(0,0)) +
    theme_tq() +
    labs(x="", y="", title = paste("Signal to Noise Ratio", sep = "")) +
    theme(legend.title = element_blank(), plot.title = element_text(hjust = 0.5, face = "bold"), 
          legend.position = "right")  
  
  p2 <- ggplot() +
    geom_line(aes(x= index(ret), y=ret[,j]), color = "red", size = 1) +
    scale_x_date(expand = c(0,0)) +
    scale_y_continuous(limits = c(-1,1),labels = scales::percent, expand = c(0,0)) +
    theme_tq() +
    labs(x="", y="", title = paste("Rolling Returns (%)", sep = "")) +
    theme(legend.title = element_blank(), plot.title = element_text(hjust = 0.5, face = "bold"), 
          legend.position = "right")
  
  g1 <- ggplotGrob(p1)
  g2 <- ggplotGrob(p2)
  
  grid.newpage()
  footnote <- "Computations: Golden Compass Quant"
  g <- grid.arrange(rbind(g1,g2), top = textGrob(nameVec[j],
                                                 gp = gpar(fontface = "bold", fontsize = 20)), 
                    bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, 
                                      gp = gpar(fontface = "italic", fontsize = 10)))
  grid.draw(g)
  
  ggsave(paste("chart", j, ".png", sep = ""),plot = g, width = 8, height = 7, dpi = 300)
}

j=4

p1 <- ggplot() +
  geom_line(aes(x= index(snr), y=snr[,j]), color = "blue", size = 1) +
  scale_x_date(expand = c(0,0)) +
  scale_y_continuous(limits = c(0,.5) ,expand = c(0,0)) +
  theme_tq() +
  labs(x="", y="", title = paste("Market Divergence Indicator", sep = "")) +
  theme(legend.title = element_blank(), plot.title = element_text(hjust = 0.5, face = "bold"), 
        legend.position = "right")  

p2 <- ggplot() +
  geom_line(aes(x= index(ret), y=ret[,j]), color = "red", size = 1) +
  scale_x_date(expand = c(0,0)) +
  scale_y_continuous(limits = c(-1,1),labels = scales::percent, expand = c(0,0)) +
  theme_tq() +
  labs(x="", y="", title = paste("Rolling Returns (%)", sep = "")) +
  theme(legend.title = element_blank(), plot.title = element_text(hjust = 0.5, face = "bold"), 
        legend.position = "right")

g1 <- ggplotGrob(p1)
g2 <- ggplotGrob(p2)

grid.newpage()
footnote <- "Computations: Golden Compass Quant"
g <- grid.arrange(rbind(g1,g2), bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, 
                                    gp = gpar(fontface = "italic", fontsize = 10)))
grid.draw(g)

ggsave(paste("chart", j, ".png", sep = ""),plot = g, width = 8, height = 7, dpi = 300)

# MDI histogram
mdih = hist(snr[,4], xlim = c(0,0.3), breaks = seq(0,0.3,0.005))
mdih$density = mdih$counts/sum(mdih$counts)*100
plot(mdih, ylim=c(0,10), main = "Histogram of MDI Values ", xlab = "100-Day MDI", ylab = "% Density", freq=FALSE)
abline(v=median(snr[,4]), col="royalblue", lwd=2)

# return histogram
reth = hist(ret[,4], xlim = c(-0.3,0.6), breaks = seq(-0.3,0.6,0.02))
reth$density = reth$counts/sum(reth$counts)*100
plot(reth, ylim=c(0,10), main = "Histogram of Rolling Returns from Trend Following System ", xlab = "100-Day Rolling Returns", ylab = "% Density", freq=FALSE)
abline(v=median(ret[,4]), col="royalblue", lwd=2)

# linear regression
df <- cbind(ret[,4],snr[,4])
colnames(df) <- c("returns","mdi")
summary(lm(returns~mdi,data = df))

# regression plot
plot(as.numeric(snr[,4]), as.numeric(ret[,4])*100, ylab="100-Day Rolling Return (%)", xlab = "MDI")
abline(lm(ret[,4]*100 ~ snr[,4]), col="blue")

# decile analysis
decile.averages <- rep(0, 10)
for(j in 1:length(decile.averages)){
  decile.averages[j] <- mean(df$returns[df$mdi<quantile(df$mdi,(j*0.1)) & df$mdi>quantile(df$mdi,((j-1)*0.1))])
}
decile.averages <- as.data.frame(decile.averages)
decile.averages$sign <- ifelse(decile.averages$decile.averages >= 0, "positive", "negative")

p <- ggplot(data = decile.averages) +
  geom_col(aes(x=index(decile.averages), y=decile.averages, fill=sign)) +
  scale_x_continuous(limits = c(0,11), breaks = 1:10, expand = c(0,0)) +
  scale_y_continuous(labels = scales::percent, limits = c(-0.25, 0.25),expand = c(0,0)) +
  scale_fill_manual(values = c("positive"="darkgreen", "negative"="red")) +
  theme_tq() +
  labs(x="MDI Decile", y = "Average 100-Day Rolling Return (%)", title = "Average Rolling Returns of Trend Following System by Decile of MDI") +
  theme(legend.title = element_blank(), legend.position = "none", plot.title = element_text(hjust = 0.5, face = "bold"))

# insert footnote
grid.newpage()
footnote <- paste("Computations: Golden Compass Quant", sep = "")
g <- arrangeGrob(p, bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, gp = gpar(fontface = "italic", fontsize = 9)))
grid.draw(g)

# save to working folder
ggsave(paste("decile avg.png", sep = ""), plot = g, width = 8, height = 6, dpi = 300)


# ROC of MDI
df$mdiroc <- ROC(df$mdi)
df$returns <- lag(df$returns)
df$mdi <- lag(df$mdi)
df <- na.omit(df)

p1 <- ggplot(data = dat.fit) +
  geom_line(aes(x= index(dat.fit), y=mdiroc), color = "red", size = 1) +
  scale_x_date(expand = c(0,0)) +
  scale_y_continuous(limits = c(-1.5,1.5),labels = scales::percent, expand = c(0,0)) +
  theme_tq() +
  labs(x="", y="", title = paste("MDI Rate of Change", sep = "")) +
  theme(legend.title = element_blank(), plot.title = element_text(hjust = 0.5, face = "bold"), 
        legend.position = "right")

p2 <- ggplot(data = dat.fit) +
  geom_line(aes(x= index(dat.fit), y=mdi), color = "blue", size = 1) +
  scale_x_date(expand = c(0,0)) +
  scale_y_continuous(limits = c(0,.5) ,expand = c(0,0)) +
  theme_tq() +
  labs(x="", y="", title = paste("Market Divergence Indicator", sep = "")) +
  theme(legend.title = element_blank(), plot.title = element_text(hjust = 0.5, face = "bold"), 
        legend.position = "right")  

g1 <- ggplotGrob(p1)
g2 <- ggplotGrob(p2)

grid.newpage()
footnote <- "Computations: Golden Compass Quant"
g <- grid.arrange(rbind(g2,g1), bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, 
                                                  gp = gpar(fontface = "italic", fontsize = 10)))
grid.draw(g)

ggsave(paste("chart", 7, ".png", sep = ""),plot = g, width = 8, height = 7, dpi = 300)


# ROC effect on return deciles
df <- na.omit(df)

roc.decile.averages <- rep(0, 10)
for(j in 1:length(roc.decile.averages)){
  roc.decile.averages[j] <- mean(df$returns[df$mdiroc<quantile(df$mdiroc,(j*0.1)) & df$mdiroc>quantile(df$mdiroc,((j-1)*0.1))])
}
roc.decile.averages <- as.data.frame(roc.decile.averages)
roc.decile.averages$sign <- ifelse(roc.decile.averages$roc.decile.averages >= 0, "positive", "negative")

p <- ggplot(data = roc.decile.averages) +
  geom_col(aes(x=index(roc.decile.averages), y=roc.decile.averages, fill=sign)) +
  scale_x_continuous(limits = c(0,11), breaks = 1:10, expand = c(0,0)) +
  scale_y_continuous(labels = scales::percent, limits = c(-0.1, 0.1),expand = c(0,0)) +
  scale_fill_manual(values = c("positive"="darkgreen", "negative"="red")) +
  theme_tq() +
  labs(x="MDI Rate of Change Decile", y = "Average 100-Day Rolling Return (%)", title = "Average Rolling Returns of Trend Following System by Decile of MDI Rate of Change") +
  theme(legend.title = element_blank(), legend.position = "none", plot.title = element_text(hjust = 0.5, face = "bold"))

# insert footnote
grid.newpage()
footnote <- paste("Computations: Golden Compass Quant", sep = "")
g <- arrangeGrob(p, bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, gp = gpar(fontface = "italic", fontsize = 9)))
grid.draw(g)

# save to working folder
ggsave(paste("roc decile avg.png", sep = ""), plot = g, width = 8, height = 6, dpi = 300)

# time series mdi roc
df$absroc <- abs(df$mdiroc)
df1 <- df
df <- df["2016/"]

p <- ggplot() +
  geom_rect(data = df, aes(xmin=as.Date(lag.xts(index(df))), xmax=as.Date(index(df)), ymin=max(df$returns)*-1.05, ymax=max(df$returns)*1.05, fill=mdiroc)) +
  geom_line(aes(x=as.Date(index(df)), y=df$returns), color="black", size=1) +
  scale_fill_gradient2(low = "red", mid = 'white', high = 'darkgreen', midpoint = 0) +
  scale_x_date(breaks = pretty_breaks(10), expand = c(0,0)) +
  scale_y_continuous(labels = comma, expand = c(0,0)) +
  labs(x=paste("Dates from ", as.Date(start.time.bars)," to ", as.Date(end.time.bars), sep = ""), y='Price', title=paste("100-Day MDI Rate of Change vs. 100-Day Rolling Returns", sep="")) +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5, face = "bold"))

# insert footnote
grid.newpage()
footnote <- paste("Computations: Golden Compass Quant", sep = "")
g <- arrangeGrob(p, bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, gp = gpar(fontface = "italic", fontsize = 9)))
grid.draw(g)

# save to working folder
ggsave(paste("Vol Adj Momentum - ", securities[i], ".png", sep = ""), plot = g, width = 8, height = 6, dpi = 300)

# event study

ret <- NULL
snr <- NULL

selection <- c(4,6,9)

for(i in selection){
  # select security
  df.prices <- bars.repo[[i]]
  df.prices <- as.xts(read.zoo(df.prices))
  colnames(df.prices) <- c("open","high","low","close","volume","oi")
  df.prices <- na.omit(df.prices$close)
  
  # signal to noise ratio
  df.prices$abs_diffs <- abs(diff(df.prices$close, lag=99))
  df.prices$sum_movements <- rollapply(df.prices$close, 100, function(z) sum(abs(diff(z)), na.rm = TRUE))
  df.prices$snr <- df.prices$abs_diffs / df.prices$sum_movements
  
  # trend following strategy
  df.prices$signal <- (EMA(df.prices$close, n=50)-EMA(df.prices$close, n=100)) / runSD(df.prices$close)
  df.prices$ret <- ROC(df.prices$close)
  df.prices$trade <- lag(ifelse(df.prices$signal>=0,1,-1)) # binary
  df.prices$pl <- df.prices$trade * df.prices$ret
  df.prices$cr <- 1+df.prices$pl
  df.prices$snr <- lag(df.prices$snr)
  df.prices <- na.omit(df.prices)
  
  
  # compare roll ret to mdi
  df.prices$cr <- cumprod(df.prices$cr)
  df.prices$rollret <- ROC(df.prices$cr, n=30)
  df.prices <- na.omit(df.prices)
  
  # print(cor(df.prices$rollret, df.prices$snr))
  ret <- cbind(ret, df.prices$rollret)
  snr <- cbind(snr, df.prices$snr)
  
}

colnames(ret) <- colnames(snr) <- names(bars.repo)[selection]

ret <- na.omit(ret)
ret$avg <- rowMeans(ret)

snr <- na.omit(snr)
snr$mdi <- rowMeans(snr)

cor(rowMeans(ret),rowMeans(snr))  

colnames(ret) <- colnames(snr) <- names(bars.repo)[selection]

ret <- na.omit(ret)
ret$avg <- rowMeans(ret)

snr <- na.omit(snr)
snr$mdi <- rowMeans(snr)

df <- cbind(ret[,4],snr[,4])
colnames(df) <- c("returns","mdi")

df$mdiroc <- (ROC(df$mdi))
df$mdi <- NULL
df$returns <- lag(df$returns,2)
df <- na.omit(df)

roc.decile.averages <- rep(0, 10)
roc.decile.median <- rep(0, 10)
roc.decile.sd <- rep(0, 10)
roc.decile.upper <- rep(0, 10)
roc.decile.lower <- rep(0, 10)
roc.decile.max <- rep(0, 10)
roc.decile.min <- rep(0, 10)

for(j in 1:length(roc.decile.averages)){
  roc.decile.upper[j] <- quantile(df$mdiroc,(j*0.1))
  roc.decile.lower[j] <- quantile(df$mdiroc,((j-1)*0.1))
  roc.decile.median[j] <- median(df$returns[df$mdiroc<quantile(df$mdiroc,(j*0.1)) & df$mdiroc>quantile(df$mdiroc,((j-1)*0.1))])
  roc.decile.averages[j] <- mean(df$returns[df$mdiroc<quantile(df$mdiroc,(j*0.1)) & df$mdiroc>quantile(df$mdiroc,((j-1)*0.1))])
  roc.decile.min[j] <- min(df$returns[df$mdiroc<quantile(df$mdiroc,(j*0.1)) & df$mdiroc>quantile(df$mdiroc,((j-1)*0.1))])
  roc.decile.max[j] <- max(df$returns[df$mdiroc<quantile(df$mdiroc,(j*0.1)) & df$mdiroc>quantile(df$mdiroc,((j-1)*0.1))])
  roc.decile.sd[j] <- sd(df$returns[df$mdiroc<quantile(df$mdiroc,(j*0.1)) & df$mdiroc>quantile(df$mdiroc,((j-1)*0.1))])
}

roc.decile.sd <- c(0.06370150,0.06719726,0.07750894,0.06730673,0.06322279,0.07874339,0.07903601,0.06301329,0.06068373,0.07751450)
roc.decile.averages <- c(-0.004076229,-0.008990552,-0.002792630,-0.008534101,0.006461085,0.015198920,0.006944151,0.011738139,0.004367267,0.011218270)

roc.decile.averages <- as.data.frame(roc.decile.averages)
roc.decile.sd <- as.data.frame(roc.decile.sd)

roc.decile.averages$sign <- ifelse(roc.decile.averages$roc.decile.averages >= 0, "positive", "negative")

p <- ggplot(data = roc.decile.averages) +
  geom_col(aes(x=index(roc.decile.averages), y=roc.decile.averages, fill=sign)) +
  scale_x_continuous(limits = c(0,11), breaks = 1:10, expand = c(0,0)) +
  scale_y_continuous(labels = scales::percent, limits = c(-0.05, 0.05),expand = c(0,0)) +
  scale_fill_manual(values = c("positive"="darkgreen", "negative"="red")) +
  theme_tq() +
  labs(x="MDI Rate of Change Decile", y = "Average 30-Day Forward Rolling Return (%)", title = "Average Forward Rolling Returns of Trend Following System\nBy Decile of MDI Rate of Change\n") +
  theme(legend.title = element_blank(), legend.position = "none", plot.title = element_text(hjust = 0.5, face = "bold"))

# insert footnote
grid.newpage()
footnote <- paste("Computations: Golden Compass Quant", sep = "")
g <- arrangeGrob(p, bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, gp = gpar(fontface = "italic", fontsize = 9)))
grid.draw(g)

# save to working folder
ggsave(paste("roc decile avg.png", sep = ""), plot = g, width = 8, height = 6, dpi = 300)

p <- ggplot(data = roc.decile.sd) +
  geom_col(aes(x=index(roc.decile.sd), y=roc.decile.sd)) +
  scale_x_continuous(limits = c(0,11), breaks = 1:10, expand = c(0,0)) +
  scale_y_continuous(labels = scales::percent, limits = c(0, 0.1),expand = c(0,0)) +
  scale_fill_manual(values = c("positive"="darkgreen", "negative"="red")) +
  theme_tq() +
  labs(x="MDI Rate of Change Decile", y = "Standard Deviation of 30-Day Forward Rolling Returns (%)", title = "Standard Deviation of Forward Rolling Returns of Trend Following System\nBy Decile of MDI ROC\n") +
  theme(legend.title = element_blank(), legend.position = "none", plot.title = element_text(hjust = 0.5, face = "bold"))

# insert footnote
grid.newpage()
footnote <- paste("Computations: Golden Compass Quant", sep = "")
g <- arrangeGrob(p, bottom = textGrob(footnote, x = 0, hjust = -0.1, vjust = 0.2, gp = gpar(fontface = "italic", fontsize = 9)))
grid.draw(g)

# save to working folder
ggsave(paste("roc decile sd.png", sep = ""), plot = g, width = 8, height = 6, dpi = 300)
